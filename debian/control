Source: python-osprofiler
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-ddt,
 python3-docutils,
 python3-elasticsearch,
 python3-hacking,
 python3-importlib-metadata,
 python3-netaddr,
 python3-openstackdocstheme,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-prettytable,
 python3-pymongo,
 python3-redis,
 python3-requests,
 python3-six,
 python3-stestr,
 python3-testtools,
 python3-webob,
 subunit,
Standards-Version: 4.1.0
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-osprofiler
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-osprofiler.git
Homepage: https://opendev.org/openstack/osprofiler

Package: python-osprofiler-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack Profiler Library - doc
 OpenStack consists of multiple projects. Each project, in turn, is composed of
 multiple services. To process some request, e.g. to boot a virtual machine,
 OpenStack uses multiple services from different projects. In the case somethin
 works too slowly, it's extremely complicated to understand what exactly goes
 wrong and to locate the bottleneck.
 .
 To resolve this issue, a tiny but powerful library, osprofiler, has been
 interoduced, and can be used by all OpenStack projects and their Python
 clients. To be able to generate one trace per request, that goes through all
 involved services, and builds a tree of calls (see an example
 http://pavlovic.me/rally/profiler/).
 .
 This package contains the documentation.

Package: python3-osprofiler
Architecture: all
Depends:
 python3-importlib-metadata,
 python3-netaddr,
 python3-oslo.concurrency,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-prettytable,
 python3-requests,
 python3-six,
 python3-webob,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Profiler Library - Python 3.x
 OpenStack consists of multiple projects. Each project, in turn, is composed of
 multiple services. To process some request, e.g. to boot a virtual machine,
 OpenStack uses multiple services from different projects. In the case somethin
 works too slowly, it's extremely complicated to understand what exactly goes
 wrong and to locate the bottleneck.
 .
 To resolve this issue, a tiny but powerful library, osprofiler, has been
 interoduced, and can be used by all OpenStack projects and their Python
 clients. To be able to generate one trace per request, that goes through all
 involved services, and builds a tree of calls (see an example
 http://pavlovic.me/rally/profiler/).
 .
 This package contains the Python 3.x module.
